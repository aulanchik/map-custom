package com.reborn.task2.api;

/**
 * @author Artyom "Reborn" Ulanchik
 */
public interface CustomMap {

    void put(Object key, Object value);

    Object get(Object key);

    boolean remove(Object key);

    int size();
}
