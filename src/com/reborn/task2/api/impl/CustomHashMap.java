package com.reborn.task2.api.impl;

import com.reborn.task2.api.CustomMap;

/**
 * @author Artyom Ulanchik
 */
public class CustomHashMap implements CustomMap {

    private Entry[] table;
    private static final int BUCKET_CAP = 100;

    static class Entry {

        Object key;

        Object value;

        Entry nextEntry;

        public Entry(Object key, Object value, Entry nextEntry) {
            this.key = key;
            this.value = value;
            this.nextEntry = nextEntry;
        }
    }

    public CustomHashMap() {
        table = new Entry[BUCKET_CAP];
    }

    @Override
    public void put(Object key, Object value) {
        int hash = hashCodeCalc(key);
        Entry newEntry = new Entry(key, value, null);
        //avoid ability to add entry with null key
        if(key == null) {
            return;
        } else if(table[hash] != null){
            Entry current = table[hash];
            current.value = newEntry.value;
        } else {
            if(table[hash] == null) {
                table[hash] = newEntry;
            } else {
                Entry last = null;
                Entry current = table[hash];
                // looping Entries until we reach the last bucket
                while(current != null) {
                    if(current.key.equals(key)) {
                        if(last == null) {
                            //update
                            newEntry.nextEntry = current.nextEntry;
                            last.nextEntry = newEntry;
                            return;
                        }
                    }
                    //reallocate links
                    last = current;
                    current = current.nextEntry;
                }
                last.nextEntry = newEntry;
            }
        }
    }


    @Override
    public Object get(Object key) {
        int hash = hashCodeCalc(key);
        if(table[hash] == null){
            return null;
        }else{
            Entry temp = table[hash];
            while(temp != null){
                if(temp.key.equals(key)) {
                    return temp.value;
                }
                //return value corresponding to key
                temp = temp.nextEntry;
            }
            //otherwise key is not found
            return null;
        }
    }

    @Override
    public boolean remove(Object key) {
         int hash = hashCodeCalc(key);
         if(table[hash] == null) {
             return false;
         } else {
             Entry last = null;
             Entry current = table[hash];
             while(current != null) {
                 if(current.key.equals(key)) {
                    if(last == null) {
                        table[hash] = table[hash].nextEntry;
                        return true;
                    }
                 } else {
                     last.nextEntry = current.nextEntry;
                     return true;
                 }
                 last = current;
             }
             return false;
         }
    }

    @Override
    public int size() {
        int size = 0;
        for(int i = 0; i < table.length ; i++) {
            if(table[i] != null) {
                size += 1;
            }
        }
        return size;
    }

    private int hashCodeCalc(Object key) {
        return Math.abs(key.hashCode()) % BUCKET_CAP;
    }

    public void showContents() {
        System.out.print("Contents: {");
        for(int i = 0; i < table.length ; i++) {
            if(table[i] != null) {
                Entry entry = table[i];
                while(entry != null) {
                    System.out.print("[ " + entry.key + " = " + entry.value + " ]");
                    entry = entry.nextEntry;
                }
            }
        }
        System.out.println("}");
    }
}
