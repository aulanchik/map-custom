package com.reborn.task2;

import com.reborn.task2.api.impl.CustomHashMap;

/**
 * Task 2.
 * Design and Implement Simple Hashmap according to the following interface:
 * @see com.reborn.task2.api.CustomMap
 *
 * Number of buckets is to be fixed to 100.
 * Hash collisions can be either handled (preferred) or ignored.
 * Hash function ought to be as simple as possible.
 * Putting another value with same key should replace the old value.
 *
 * @author Artyom "Reborn" Ulanchik
 */
public class Demo {

    public static void main(String[] args) {
        CustomHashMap customHashMap = new CustomHashMap();
        customHashMap.put(21, 12);
        customHashMap.put(25, 2);
        customHashMap.put(30, 23);
        customHashMap.put(33, 75);
        customHashMap.put(35, 79);

        System.out.println("Value for key '21'= " + customHashMap.get(21));
        System.out.println("Value for key '51'= " + customHashMap.get(51));
        customHashMap.showContents();
        System.out.println("Current size: " + customHashMap.size());

        System.out.println("\nUpdate value to '233' for key 21");
        customHashMap.put(21, 233);
        customHashMap.showContents();

        System.out.println("\nUpdate value to '777' for key 33");
        customHashMap.put(33, 777);
        customHashMap.showContents();

        System.out.println("\nRemove key '21'");
        System.out.println("Removed? " + customHashMap.remove(21));
        customHashMap.showContents();

        System.out.println("\nRemove key '66'");
        System.out.println("Removed? " + customHashMap.remove(66));
        customHashMap.showContents();
    }
}
