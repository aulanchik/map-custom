# README #

This is a qualified skill assesment test for Java Developer position

## Task Description ##
Design and Implement Simple Hashmap according to the following interface:

```
#!java

public interface CustomMap {
 void put(Object key, Object value);
 Object get(Object key);
 Object remove(Object key);
 int size();
}
```
## Restrictions ##
* Number of buckets is to be fixed to 100.
* Hash collisions can be either handled (preferred) or ignored.
* Hash function ought to be as simple as possible.
* Putting another value with same key should replace the old value.

# System Requirements #

 * Java 1.7 JDK/JRE

# How to launch? #

In project root folder execute 'executeTask2.bat' file.